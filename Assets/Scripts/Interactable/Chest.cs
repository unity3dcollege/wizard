﻿using System;
using UnityEngine;

public class Chest : MonoBehaviour, IInteractable
{
    [SerializeField]
    private float interactRange = 1.5f;
    [SerializeField]
    private Weapon weaponPrefab;

    private bool canOpen = true;

    public float InteractRange { get { return interactRange; } }

    public bool CanInteract()
    {
        return canOpen;
    }

    public void Interact(Brain brain)
	{
        canOpen = false;

        GetComponentInChildren<Animator>().SetTrigger("Open");

        Weapon weapon = Instantiate(weaponPrefab);

        brain.GetComponent<Inventory>().AddItem(weapon);
	}
}
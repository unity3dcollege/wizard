﻿public interface IInteractable : ITargetable
{
    bool CanInteract();
    void Interact(Brain brain);
    float InteractRange { get; }
}
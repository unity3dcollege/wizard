﻿public interface IAttackable : ITargetable
{
    void TakeDamage(int damage);
}
﻿using UnityEngine;

public interface ITargetable
{
    Transform transform { get; }
    GameObject gameObject { get; }
}
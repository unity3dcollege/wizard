﻿using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 Random(float min, float max)
    {
        return new Vector3(
            UnityEngine.Random.Range(min, max),
            UnityEngine.Random.Range(min, max),
            UnityEngine.Random.Range(min, max)
        );
    }

    public static Vector3 With(this Vector3 original, 
        float? x = null, 
        float? y = null, 
        float? z = null)
    {
        return new Vector3(x ?? original.x, y ?? original.y, z ?? original.z);
    }
}
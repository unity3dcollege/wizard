﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class WeaponButton : MonoBehaviour
{
    private Button button;
    private KeyCode hotkey;
    private HotkeyPanel hotkeyPanel;
    private int siblingIndex;

    [SerializeField]
    private Image weaponSprite;
    [SerializeField]
    private Text numberText;

    private void Awake()
    {
        hotkeyPanel = GetComponentInParent<HotkeyPanel>();
        button = GetComponent<Button>();
        siblingIndex = transform.GetSiblingIndex();
        hotkey = KeyCode.Alpha1 + siblingIndex;

        button.onClick.AddListener(EquipWeapon);

        numberText.text = (siblingIndex + 1).ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(hotkey))
        {
            EquipWeapon();
        }
    }

    public void SetWeapon(Weapon weapon)
    {
        if (weapon != null)
        {
            weaponSprite.sprite = weapon.Sprite;
            weaponSprite.enabled = true;
        }
        else
        {
            weaponSprite.enabled = false;
        }
    }

    private void EquipWeapon()
    {
        Debug.Log("Equip Weapon " + hotkey);
        hotkeyPanel.TryEquipItem(siblingIndex);
    }
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    private Animator animator;
    private Health health;
    private Slider slider;
    private bool isHoveringHealthBar = true;
    private Image[] images;
    private bool visible;
    private float stayVisibleUntilTime;

    [SerializeField]
    private float heightOffset = 2f;
    [SerializeField]
    private float visibleTimeAfterHit = 4f;
    

    private void Awake()
    {
        animator = GetComponent<Animator>();
        slider = GetComponent<Slider>();
        images = GetComponentsInChildren<Image>();
    }

    private void Start()
    {
        health = GetComponentInParent<Health>();

        if (health == null)
        {
            health = Player.Instance.GetComponent<Health>();
            isHoveringHealthBar = false;
        }

        health.OnHealthChanged += Health_OnHealthChanged;
    }

    private void Update()
    {
        if (isHoveringHealthBar)
            HoverHealthBar();
    }

    private void HoverHealthBar()
    {
        Vector3 worldPosition = health.transform.position + Vector3.up * heightOffset;
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);
        transform.position = screenPosition;
    }

    private void Health_OnHealthChanged(Health.HealthChangeData data)
    {
        slider.value = data.pct;

        if (animator != null)
        {
            stayVisibleUntilTime = Time.time + visibleTimeAfterHit;

            if (visible == false)
                StartCoroutine(ShowHPBar());
        }
    }

    private IEnumerator ShowHPBar()
    {
        visible = true;
        animator.SetBool("Visible", true);

        while (stayVisibleUntilTime > Time.time)
            yield return null;

        animator.SetBool("Visible", false);
        visible = false;
    }
}
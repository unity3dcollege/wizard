﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthChangeText : MonoBehaviour
{
    [SerializeField]
    private float floatSpeed = 50f;

    [SerializeField]
    private float heightOffset = 2.5f;

    private Health health;

    private TextMeshProUGUI[] textChildren;
    private Queue<TextMeshProUGUI> textQueue;

    private void Awake()
    {
        InitializeTextChildren();

        health = GetComponentInParent<Health>();
        health.OnHealthChanged += Health_OnHealthChanged;
    }

    private void InitializeTextChildren()
    {
        textChildren = GetComponentsInChildren<TextMeshProUGUI>();
        textQueue = new Queue<TextMeshProUGUI>(textChildren);
        for (int i = 0; i < textChildren.Length; i++)
        {
            textChildren[i].gameObject.SetActive(false);
        }
    }

    private void Health_OnHealthChanged(Health.HealthChangeData data)
    {
        TextMeshProUGUI textToUse = textQueue.Dequeue();
        textToUse.text = "-" + data.delta.ToString();

        textToUse.gameObject.SetActive(true);
        textToUse.StartCoroutine(FloatAndFade(textToUse));
    }
    
    private IEnumerator FloatAndFade(TextMeshProUGUI textToUse)
    {
        MoveTextToCorrectPosition(textToUse);

        float elapsed = 0f;
        while (elapsed < 1f)
        {
            textToUse.transform.position += Vector3.up * Time.deltaTime * floatSpeed;
            elapsed += Time.deltaTime;
            yield return null;
        }

        textToUse.transform.localPosition = Vector3.zero;
        textQueue.Enqueue(textToUse);
        textToUse.gameObject.SetActive(false);
    }

    private void MoveTextToCorrectPosition(TextMeshProUGUI textToUse)
    {
        Vector3 worldPosition = health.transform.position + Vector3.up * heightOffset;
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);
        transform.position = screenPosition;
    }
}
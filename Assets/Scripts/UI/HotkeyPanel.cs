﻿using System;
using UnityEngine;

public class HotkeyPanel : MonoBehaviour
{
    private Inventory inventory;
    private WeaponButton[] weaponButtons;

    private void Awake()
    {
        weaponButtons = GetComponentsInChildren<WeaponButton>();
    }

    private void Start()
    {
        BindToInventory(Player.Instance.GetComponent<Inventory>());
    }

    private void BindToInventory(Inventory inventoryToBind)
    {
        if (inventory != null)
            inventory.OnInventoryChanged -= Refresh;

        inventory = inventoryToBind;
        inventory.OnInventoryChanged += Refresh;

        Refresh();
    }

    private void Refresh()
    {
        var items = inventory.GetItems();
        for (int i = 0; i < items.Count; i++)
        {
            weaponButtons[i].SetWeapon(items[i]);
        }
        for (int i = items.Count; i < weaponButtons.Length; i++)
        {
            weaponButtons[i].SetWeapon(null);
        }
    }

    public void TryEquipItem(int index)
    {
        bool equipWasSuccessful = inventory.EquipItem(index);
    }
}
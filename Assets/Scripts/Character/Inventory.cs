﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField]
    private Weapon defaultWeaponPrefab;
    [SerializeField]
    private Transform weaponLocation;

    private List<Weapon> items = new List<Weapon>();
    private Transform inventoryRoot;

    public Weapon CurrentWeapon { get; private set; }

    public event Action OnInventoryChanged = delegate { };

    private void Awake()
    {
        inventoryRoot = new GameObject("Inventory").transform;
        inventoryRoot.SetParent(transform);
        inventoryRoot.localPosition = Vector3.zero;

        AddDefaultWeapon();
    }

    public List<Weapon> GetItems()
    {
        return new List<Weapon>(items);
    }

    public bool EquipItem(int index)
    {
        if (items.Count > index)
        {
            Equip(items[index]);
            return true;
        }

        return false;
    }

    private void AddDefaultWeapon()
    {
        if (defaultWeaponPrefab != null)
        {
            Weapon defaultWeapon = Instantiate(defaultWeaponPrefab);
            AddItem(defaultWeapon);
            Equip(defaultWeapon);
        }
    }

    public void AddItem(Weapon item)
    {
        items.Add(item);
        item.transform.SetParent(inventoryRoot);
        item.gameObject.SetActive(false);

        OnInventoryChanged();
    }

    private void Equip(Weapon weapon)
    {
        if (CurrentWeapon != null)
            UnEquip();

        CurrentWeapon = weapon;
        CurrentWeapon.transform.SetParent(weaponLocation);
        CurrentWeapon.transform.localPosition = Vector3.zero;
        CurrentWeapon.transform.localRotation = Quaternion.identity;
        CurrentWeapon.gameObject.SetActive(true);
    }

    private void UnEquip()
    {
        CurrentWeapon.transform.SetParent(inventoryRoot);
        CurrentWeapon.gameObject.SetActive(false);
    }
}
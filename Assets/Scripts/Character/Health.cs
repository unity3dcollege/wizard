﻿using System;
using UnityEngine;

public class Health : MonoBehaviour, IAttackable
{
    [SerializeField]
    private int maxHealth = 5;
    
    private int health;

    public event Action OnTookDamage = delegate { };
    public event Action<HealthChangeData> OnHealthChanged = delegate { };

    public struct HealthChangeData
    {
        public int current;
        public int max;
        public int delta;
        public float pct;
    }

    private void Awake()
    {
        health = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        OnTookDamage();
        SendHealthChanged(damage);

        if (health <= 0)
            Destroy(gameObject);
    }

    private void SendHealthChanged(int damage)
    {
        float pct = (float)this.health / (float)this.maxHealth;

        HealthChangeData data = new HealthChangeData()
        {
            current = this.health,
            max = this.maxHealth,
            delta = damage,
            pct = pct
        };

        OnHealthChanged(data);
    }
}
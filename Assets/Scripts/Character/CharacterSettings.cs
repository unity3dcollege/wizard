﻿using UnityEngine;

public class CharacterSettings : MonoBehaviour
{
    [SerializeField]
    private float aggroRange = 5f;
    public float AggroRange { get { return aggroRange; } }

    [SerializeField]
    private float stopChaseRange = 10f;
    public float StopChaseRange { get { return stopChaseRange; } }

    [SerializeField]
    private float wanderRange = 0f;
    public float WanderRange { get { return wanderRange; } }

    private void OnDrawGizmos()
    {
        if (wanderRange != 0f)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, wanderRange);
        }
    }
}
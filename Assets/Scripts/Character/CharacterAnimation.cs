﻿using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class CharacterAnimation : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator animator;
    private Brain brain;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        brain = GetComponent<Brain>();
        brain.OnAttackStarted += HandleAttackStarted;
    }

    private void HandleAttackStarted(Weapon weapon)
    {
        if (weapon is MeleeWeapon)
            animator.SetTrigger("Attack");
        else if (weapon is ProjectileWeapon)
            animator.SetTrigger("Cast");
    }

    private void Update()
    {
        float speed = agent.velocity.magnitude;
        animator.SetFloat("Speed", speed);
    }
}
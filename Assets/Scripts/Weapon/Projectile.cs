﻿using System;
using UnityEngine;

public class Projectile : PooledMonoBehaviour
{
    private ITargetable target;

    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float hitRange = 2f;
    [SerializeField]
    private int damage = 1;

    internal void FireAt(ITargetable target)
    {
        this.target = target;
    }

    private void Update()
    {
        if (target == null || (target as UnityEngine.Object) == null)
        {
            AddToPool();
        }
        else if (HitTarget())
        {
            DamageTarget();
            AddToPool();
        }
        else
        {
            MoveTowardTarget();
        }
    }

    private void AddToPool()
    {
        gameObject.SetActive(false);
    }

    private void DamageTarget()
    {
        target.gameObject.GetComponent<Health>().TakeDamage(damage);
    }

    private bool HitTarget()
    {
        return Vector3.Distance(transform.position, target.transform.position) < hitRange;
    }

    private void MoveTowardTarget()
    {
        Vector3 directionToTarget = Vector3.Normalize(target.transform.position - transform.position);
        transform.position += directionToTarget * Time.deltaTime * speed;
    }
}
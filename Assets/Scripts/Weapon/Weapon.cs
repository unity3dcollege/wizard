﻿using System;
using System.Collections;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    private float lastAttackTime;

    [SerializeField]
    protected int damage = 1;

    [SerializeField]
    protected float attackDelay = 0.5f;

    [SerializeField]
    private float range = 5f;
    public float Range { get { return range; } }

    [SerializeField]
    private Sprite sprite;
    public Sprite Sprite { get { return sprite; } }

    protected abstract IEnumerator DoAttack(IAttackable target);

    public event Action OnAttack = delegate { };

    public bool IsReady()
    {
        return Time.time - lastAttackTime > attackDelay;
    }

    internal void AttackTarget(IAttackable target)
    {
        OnAttack();

        lastAttackTime = Time.time;
        StartCoroutine(DoAttack(target));
    }
}
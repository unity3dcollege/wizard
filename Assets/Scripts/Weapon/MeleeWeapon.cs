﻿using System.Collections;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    [SerializeField]
    private float delayBeforeDamage = 0.2f;

    protected override IEnumerator DoAttack(IAttackable target)
    {
        yield return new WaitForSeconds(delayBeforeDamage);

        target.TakeDamage(damage);
    }

    private void OnValidate()
    {
        if (delayBeforeDamage > attackDelay && attackDelay > 0f)
        {
            delayBeforeDamage = attackDelay - 0.1f;
            delayBeforeDamage = Mathf.Max(delayBeforeDamage, 0f);
        }
    }
}
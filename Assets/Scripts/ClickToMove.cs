﻿using UnityEngine;
using UnityEngine.AI;

public class ClickToMove : MonoBehaviour
{
    private PlayerBrain brain;

    private void Awake()
    {
        brain = GetComponent<PlayerBrain>();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(Camera.main.transform.position, ray.direction * 50f, Color.red);

            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                ITargetable targetable = hitInfo.collider.GetComponent<ITargetable>();

                if (targetable != null)
                {
                    brain.TrySwitchState(new ChaseTargetable(brain, targetable));
                }
                else
                {
                    brain.TrySwitchState(new MoveToPosition(brain, hitInfo.point));
                }
            }
        }
    }
}
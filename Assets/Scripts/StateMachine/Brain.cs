﻿using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(CharacterSettings))]
public class Brain : MonoBehaviour
{
    private CharacterState currentState;

    public CharacterState CurrentState { get { return currentState; } }

    public Action<Weapon> OnAttackStarted = delegate { };

    public virtual CharacterState DefaultState()
    {
        return new WaitForAggro(this);
    }

    private void Awake()
    {
        SwitchState(DefaultState());
    }

    private void Update()
    {
        if (currentState != null)
        {
            currentState.Tick();

            if (currentState.NextState != null)
            {
                SwitchState(currentState.NextState);
            }
        }
    }

    protected void SwitchState(CharacterState newState)
    {
        if (currentState != null)
            currentState.ExitState();

        currentState = newState;

        currentState.EnterState();
    }

    public override string ToString()
    {
        if (currentState != null)
            return currentState.ToString();

        return "NO STATE";
    }
}
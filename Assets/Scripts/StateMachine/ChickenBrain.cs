﻿using UnityEngine;

public class ChickenBrain : Brain
{
    public override CharacterState DefaultState()
    {
        return new Wander(this);
    }
}
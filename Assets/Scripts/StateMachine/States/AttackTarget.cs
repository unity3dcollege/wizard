﻿using System;
using UnityEngine;

public class AttackTarget : CharacterState
{
    private IAttackable target;
    private float lastAttackTime;
    private float attackDelay = 1.5f;

    public AttackTarget(Brain brain, IAttackable target) : base(brain)
    {
        this.target = target;
    }

    public override void EnterState()
    {
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (target == null || (target as UnityEngine.Object) == null)
        {
            NextState = brain.DefaultState();
            return;
        }

        float distance = Vector3.Distance(target.transform.position, transform.position);
        if (distance > weapon.Range)
            NextState = new ChaseTargetable(brain, target);
        else
            TryAttack();
    }

    private void TryAttack()
    {
        if (weapon.IsReady())
        {
            weapon.AttackTarget(target);
            brain.OnAttackStarted(weapon);
        }
    }
}
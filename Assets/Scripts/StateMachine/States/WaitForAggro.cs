﻿using System;
using UnityEngine;

public class WaitForAggro : CharacterState
{
    private float distance;

    public WaitForAggro(Brain brain) : base(brain)
    {
    }

    public override void EnterState()
    {
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (InRangeOfPlayer())
            NextState = new ChaseTargetable(brain, Player.Instance.Health);
    }

    private bool InRangeOfPlayer()
    {
        distance = Vector3.Distance(brain.transform.position, Player.Instance.transform.position);

        return distance < settings.AggroRange;
    }

    public override string ToString()
    {
        return string.Format("WaitForAggro {0:0.0}/{1}", distance, settings.AggroRange);
    }
}
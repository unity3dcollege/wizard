﻿using UnityEngine;

public class MoveToPosition : CharacterState
{
    private Vector3 destination;
    public Vector3 Destination { get { return destination; } }

    public MoveToPosition(Brain brain, Vector3 destination) : base(brain)
    {
        this.destination = destination;
    }

    public override void EnterState()
    {
        navmeshagent.SetDestination(Destination);
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
    }
}
﻿using System;
using UnityEngine;

public class GrowWhenClose : CharacterState
{
    private float startingScale;

    public GrowWhenClose(Brain brain) : base(brain)
    {
    }

    public override void EnterState()
    {
        startingScale = transform.localScale.magnitude;
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (Vector3.Distance(transform.position, Player.Instance.transform.position) < 5f)
        {
            Grow();
        }
        else
        {
            Shrink();
        }
    }

    private void Shrink()
    {
        if (transform.localScale.magnitude > startingScale )
            transform.localScale *= 0.99f;
    }

    private void Grow()
    {
        if (transform.localScale.magnitude < startingScale * 2f)
            transform.localScale *= 1.01f;
    }
}
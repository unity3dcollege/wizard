﻿using UnityEngine;
using UnityEngine.AI;

internal class ChaseTargetable : CharacterState
{
    private float distance;
    private ITargetable target;

    public ChaseTargetable(Brain brain, ITargetable target) : base(brain)
    {
        this.target = target;
    }

    public override void EnterState()
    {
    }

    public override void ExitState()
    {
        brain.GetComponent<NavMeshAgent>().SetDestination(brain.transform.position);
    }

    public override void Tick()
    {
        if (target == null || (target as UnityEngine.Object) == null)
        {
            NextState = brain.DefaultState();
            return;
        }

        distance = Vector3.Distance(brain.transform.position, target.transform.position);

        if (target is IAttackable)
        {
            Attack();
        }
        else if (target is IInteractable)
        {
            Interact();
        }

        navmeshagent.SetDestination(target.transform.position);
    }

    private void Interact()
    {
        IInteractable interactable = target as IInteractable;
        if (interactable.CanInteract() && distance <= interactable.InteractRange)
        {
            interactable.Interact(brain);
            NextState = brain.DefaultState();
        }
    }

    private void Attack()
    {
        if (distance > settings.StopChaseRange)
        {
            NextState = new WaitForAggro(brain);
        }
        else
        {
            if (distance <= weapon.Range)
                NextState = new AttackTarget(brain, target as IAttackable);
        }
    }

    public override string ToString()
    {
        return string.Format("ChaseTargetable {0:0.0}/{1}", distance, settings.StopChaseRange);
    }
}
﻿using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
public class BrainDebugger : MonoBehaviour
{
    private Brain brain;
    private NavMeshAgent navmeshagent;

    private Vector3 offset = Vector3.up * 2f;

    public Vector3[] pathPoints;

    private void Awake()
    {
        brain = GetComponent<Brain>();
        navmeshagent = GetComponent<NavMeshAgent>();
    }

    private void OnDrawGizmos()
    {
        Handles.Label(transform.position + offset, brain.ToString(),
            new GUIStyle()
            {
                normal = new GUIStyleState() { textColor = Color.red },
                fontSize = 20,
                alignment = TextAnchor.MiddleCenter
            }
        );

        if (brain.CurrentState is MoveToPosition)
        {
            Gizmos.DrawWireSphere((brain.CurrentState as MoveToPosition).Destination, 0.25f);
        }

        if (navmeshagent.path.status == NavMeshPathStatus.PathComplete && navmeshagent.path.corners.Length > 1)
        {
            for (int i = 1; i < navmeshagent.path.corners.Length; i++)
            {
                Gizmos.DrawLine(navmeshagent.path.corners[i - 1],
                    navmeshagent.path.corners[i]);
            }
        }

        pathPoints = navmeshagent.path.corners;
    }
}
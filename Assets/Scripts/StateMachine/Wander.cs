﻿using System;
using UnityEngine;

public class Wander : CharacterState
{
    private Vector3 startPosition;

    public Wander(Brain brain) : base(brain)
    {
    }

    public override void EnterState()
    {
        startPosition = transform.position;
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (DestinationNeeded())
        {
            ChooseRandomDestination();
        }
    }

    private bool DestinationNeeded()
    {
        return navmeshagent.desiredVelocity.magnitude <= 0f;
    }

    private void ChooseRandomDestination()
    {
        Vector3 randomOffset = Vector3Extensions.Random(-settings.WanderRange, settings.WanderRange);
        randomOffset = randomOffset.With(y: 10f);

        Vector3 randomPosition = startPosition + randomOffset;

        float distance = 15f;
        int layermask = LayerMask.GetMask("Ground");

        Debug.DrawRay(randomPosition, Vector3.down * distance, Color.red);

        RaycastHit hitInfo;
        if (Physics.Raycast(randomPosition, Vector3.down, out hitInfo, distance, layermask))
        {
            navmeshagent.SetDestination(hitInfo.point);
        }
    }
}
﻿using UnityEngine;
using UnityEngine.AI;

public abstract class CharacterState
{
    protected Brain brain;
    protected NavMeshAgent navmeshagent;
    protected Transform transform;
    protected CharacterSettings settings;
    protected Inventory inventory;

    protected Weapon weapon { get { return inventory.CurrentWeapon; } }

    public CharacterState(Brain brain)
    {
        this.brain = brain;
        this.navmeshagent = brain.GetComponent<NavMeshAgent>();
        this.transform = brain.transform;
        this.settings = brain.GetComponent<CharacterSettings>();
        this.inventory = brain.GetComponent<Inventory>();
    }

    public abstract void EnterState();
    public abstract void ExitState();
    public abstract void Tick();

    public CharacterState NextState { get; set; }
}
﻿using UnityEngine;

public class PlayerBrain : Brain
{
    public void TrySwitchState(CharacterState state)
    {
        SwitchState(state);
    }

    public override CharacterState DefaultState()
    {
        return new Idle(this);
    }
}
﻿using System;
using System.Collections;
using UnityEngine;

public class HitDebugger : MonoBehaviour
{
    private Renderer meshrenderer;

    private void Awake()
    {
        meshrenderer = GetComponent<Renderer>();
        GetComponent<Health>().OnTookDamage += HitDebugger_OnTookDamage;
    }

    private void HitDebugger_OnTookDamage()
    {
        StartCoroutine(FlashRed());
    }

    private IEnumerator FlashRed()
    {
        meshrenderer.material.color = Color.red;
        yield return new WaitForSeconds(0.25f);
        meshrenderer.material.color = Color.white;
    }
}
﻿using System.Collections;
using UnityEngine;

public class AttackDebugger : MonoBehaviour
{
    private Renderer meshrenderer;

    private void Awake()
    {
        meshrenderer = GetComponent<Renderer>();
        GetComponent<Weapon>().OnAttack += AttackDebugger_OnAttack;
    }

    private void AttackDebugger_OnAttack()
    {
        StartCoroutine(FlashRed());
    }

    private IEnumerator FlashRed()
    {
        meshrenderer.material.color = Color.green;
        yield return new WaitForSeconds(0.25f);
        meshrenderer.material.color = Color.white;
    }
}
﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    private Health health;

    public Health Health { get { return health; } }

    private void Awake()
    {
        Instance = this;
        health = GetComponent<Health>();
    }
}